package com.kz.fm_server.mapper;

public class UserMapper {

	private String sUserName;
	private String sPassword;
	private String sAccountType;
	private String sEmail;
	private String validate;
	
	public String getsUserName() {
		return sUserName;
	}
	public void setsUserName(String sUserName) {
		this.sUserName = sUserName;
	}
	public String getsPassword() {
		return sPassword;
	}
	public void setsPassword(String sPassword) {
		this.sPassword = sPassword;
	}
	public String getsAccountType() {
		return sAccountType;
	}
	public void setsAccountType(String sAccountType) {
		this.sAccountType = sAccountType;
	}
	public String getsEmail() {
		return sEmail;
	}
	public void setsEmail(String sEmail) {
		this.sEmail = sEmail;
	}
	public String getValidate() {
		return validate;
	}
	public void setValidate(String validate) {
		this.validate = validate;
	}
	
	
	
}
