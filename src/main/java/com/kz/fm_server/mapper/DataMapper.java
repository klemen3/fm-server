package com.kz.fm_server.mapper;

import java.util.ArrayList;

public class DataMapper {
	
	private ArrayList<String> lCategories;
	private String sType;
	private float fValue;
	private String sCategory;
	private String sDescription;
	private String sDate;
	private double dMonthlyBudgetSetting;
	private double dTotalBalance;
	private double dMonthlyBalance;
	private double dMonthlyBudget;

	public ArrayList<String> getlCategories() {
		return lCategories;
	}

	public void setlCategories(ArrayList<String> lCategories) {
		this.lCategories = lCategories;
	}

	public String getsType() {
		return sType;
	}

	public void setsType(String sType) {
		this.sType = sType;
	}

	public float getfValue() {
		return fValue;
	}

	public void setfValue(float fValue) {
		this.fValue = fValue;
	}

	public String getsCategory() {
		return sCategory;
	}

	public void setsCategory(String sCategory) {
		this.sCategory = sCategory;
	}

	public String getsDescription() {
		return sDescription;
	}

	public void setsDescription(String sDescription) {
		this.sDescription = sDescription;
	}

	public String getsDate() {
		return sDate;
	}

	public void setsDate(String sDate) {
		this.sDate = sDate;
	}

	public double getdMonthlyBudgetSetting() {
		return dMonthlyBudgetSetting;
	}

	public void setdMonthlyBudgetSetting(double dMonthlyBudgetSetting) {
		this.dMonthlyBudgetSetting = dMonthlyBudgetSetting;
	}

	public double getdTotalBalance() {
		return dTotalBalance;
	}

	public void setdTotalBalance(double dTotalBalance) {
		this.dTotalBalance = dTotalBalance;
	}

	public double getdMonthlyBalance() {
		return dMonthlyBalance;
	}

	public void setdMonthlyBalance(double dMonthlyBalance) {
		this.dMonthlyBalance = dMonthlyBalance;
	}

	public double getdMonthlyBudget() {
		return dMonthlyBudget;
	}

	public void setdMonthlyBudget(double dMonthlyBudget) {
		this.dMonthlyBudget = dMonthlyBudget;
	}
	
	
	

}
