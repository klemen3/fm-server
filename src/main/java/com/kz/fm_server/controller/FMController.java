package com.kz.fm_server.controller;


import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kz.fm_server.dao.DAO;
import com.kz.fm_server.mapper.DataMapper;
import com.kz.fm_server.mapper.UserMapper;
import com.kz.fm_server.service.LoginService;


@RestController
public class FMController {

	private final static Logger LOGGER = Logger.getLogger(FMController.class.getName());
	
	@Autowired
	private DAO dao;
	
	@Autowired
	private LoginService loginService;
	
	@RequestMapping(value 	 = "/saveTransaction", 
					method   = RequestMethod.POST, 
					consumes = MediaType.APPLICATION_JSON_VALUE)
	public void saveTransaction(@RequestBody DataMapper mapper) {
		LOGGER.info("FMController - saveTransaction");
		String sTransactionType = mapper.getsType();
		float fValue = mapper.getfValue();
		String sCategory = mapper.getsCategory();
		String sDescription = mapper.getsDescription();
		String sDate = mapper.getsDate();

		if (sTransactionType.equals("Income")) {
			dao.insert(fValue, sCategory, sDescription, sDate);
			LOGGER.info("Income");
		} else {
			dao.insert(-fValue, sCategory, sDescription, sDate);
			LOGGER.info("Expense");
		}
	}
	
	@RequestMapping(value    = "/refreshData", 
					method   = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	public HashMap<String, Double> refreshData() {
		LOGGER.info("FMController - refreshData");
		HashMap<String, Double> hmData = new HashMap<>();
		hmData.put("dTotalBalance",   dao.selectTotalBalance());
		hmData.put("dMonthlyBalance", dao.selectMonthlyBalance());
		hmData.put("dMonthlyBudget",  dao.monthlyBudgetLeft());
		
		return hmData;
		
	}
	
	@RequestMapping(value    = "/saveSettings", 
					method   = RequestMethod.POST,
					consumes = MediaType.APPLICATION_JSON_VALUE,
					produces = MediaType.APPLICATION_JSON_VALUE)
	public HashMap<String, Double> saveSettings(@RequestBody DataMapper mapper) {
		LOGGER.info("FMController - saveSettigns");
		double dBudget = mapper.getdMonthlyBudgetSetting();
		System.out.println("FMController dBudget: " + dBudget);
		dao.insertSettings(dBudget);
		HashMap<String, Double> hmData = new HashMap<>();
		double dMoneyLeft = dao.monthlyBudgetLeft();
		hmData.put("budget", dMoneyLeft);
		LOGGER.info("Monthly budget left: " + dMoneyLeft);
		
		return hmData;
	}
	
	@RequestMapping(value    = "/getMonthlyBudgetSetting", 
					method   = RequestMethod.GET, 
					produces = MediaType.APPLICATION_JSON_VALUE)
	public HashMap<String, Double> getMonthlyBudgetSetting() {
		LOGGER.info("FMController - getMonthlyBudgetSetting");
		HashMap<String, Double> hmData = new HashMap<>();
		double dMonthlyBudgetSetting = dao.getMonthlyBudgetSetting();
		hmData.put("dMonthlyBudgetSetting", dMonthlyBudgetSetting);
		LOGGER.info("Monthly budget setting: " + dMonthlyBudgetSetting);
		
		return hmData;
	}
	
	@RequestMapping(value = "/connect", method = RequestMethod.GET)
	@EventListener(ApplicationReadyEvent.class)
	public void connect() {
		dao.connect();
	}

	@RequestMapping(value    = "/getCategories",
			   		method   = RequestMethod.GET,
			   		produces = MediaType.APPLICATION_JSON_VALUE)
	public HashMap<String, ArrayList<String>> getCategories() {
		LOGGER.info("FMController - getCategories");
		ArrayList<String> aCategories = dao.getCategories();
		HashMap<String, ArrayList<String>> hmData = new HashMap<>();
		hmData.put("aCategories", aCategories);
		return hmData;
	}
	
	@RequestMapping(value    = "/getPercentages",
	   			    method   = RequestMethod.GET,
	   			    produces = MediaType.APPLICATION_JSON_VALUE)
	public HashMap<String, HashMap<String, Double>> getPercentages() {
		LOGGER.info("FMController - getPercentages");
		HashMap<String, HashMap<String, Double>> hmData = dao.getPercentages();
		
		return hmData;
	}
	
	
	// TODO ?
//	@RequestMapping(value    = "/androidLogin",
//					consumes = MediaType.APPLICATION_JSON_VALUE,
//					produces = MediaType.APPLICATION_JSON_VALUE)
//	public HashMap<String, String> androidLogin(@RequestBody UserMapper mapper) throws ClassNotFoundException, SQLException, PropertyVetoException {
//		HashMap<String, String> hmData = new HashMap<>();
//		boolean bValidate = loginService.check(mapper.getsUserName(), mapper.getsPassword());
//		if (bValidate) {
//			hmData.put("validate", "true");
//		} else {
//			hmData.put("validate", "false");
//		}
//		return hmData;
//	}
	
	
}
