package com.kz.fm_server.controller;

import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kz.fm_server.mapper.UserMapper;
import com.kz.fm_server.service.LoginService;

@RestController
public class LoginController {
	
	private static final Logger LOGGER = Logger.getLogger(LoginController.class.getName());
	
	@Autowired
	private LoginService loginService;
	
	@PostMapping(value = "/login")
	public HashMap<String, String> checkUser(@RequestBody UserMapper mapper) 
							      					 throws ClassNotFoundException, 
							      					 		SQLException, 
							      					 		PropertyVetoException {

		LOGGER.info("Server login controller");
		
		
		
		boolean bCheckCredentials = loginService.check(mapper.getsUserName(), mapper.getsPassword());
		
		LOGGER.info("Check: " + bCheckCredentials);
		
		HashMap<String, String> hmData = new HashMap<>();
		
		if (bCheckCredentials) {
			hmData.put("validate", "true");
		} else {
			hmData.put("validate", "false");
		}
		
		return hmData;
		
	}
}
