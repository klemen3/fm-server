package com.kz.fm_server.dao;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mchange.v2.c3p0.ComboPooledDataSource;


@Component // TODO: dokoncaj
public class ConnectionPoolDataSource {
	
	private final Logger LOGGER = Logger.getLogger(ConnectionPoolDataSource.class.getName());
	
	@Value("${datasource.url}")
	private String sUrl;
	@Value("${datasource.driverName}")
	private String sDriverName;
	@Value("${datasource.username}")
	private String sUsername;
	@Value("${datasource.password}")
	private String sPassword;

	private ComboPooledDataSource comboPooledDataSource;
	
	@PostConstruct
	public void init() throws PropertyVetoException {
		comboPooledDataSource = new ComboPooledDataSource();
		comboPooledDataSource.setDriverClass(sDriverName);
		comboPooledDataSource.setJdbcUrl(sUrl);
		comboPooledDataSource.setUser(sUsername);
		comboPooledDataSource.setPassword(sPassword);
	}
	
	public Connection getConnection() throws SQLException {
		LOGGER.info("URL: " 	 + this.sUrl);
		LOGGER.info("Driver: "   + this.sDriverName);
		LOGGER.info("Username: " + this.sUsername);
		LOGGER.info("Password: " + this.sPassword);
		LOGGER.info("Getting connection...");
		return this.comboPooledDataSource.getConnection();
	}
	
}
