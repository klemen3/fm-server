package com.kz.fm_server.dao;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class DAO {
	
	private final Logger LOGGER = Logger.getLogger(DAO.class.getName());
	
	private Connection conn          = null;
	private Statement stmt           = null;
	private CallableStatement cstmt  = null;
	private PreparedStatement pstmt  = null;	
	private ResultSet rs  		     = null;

	// Queries
	@Value("${selectMonthlyBalanceQuery}")
	private String qSelectMonthlyBalance;
	@Value("${selectMonthlyBudgetQuery}")
	private String qSelectMonthlyBudget;
	@Value("${getBudgetQuery}")
	private String qGetBudgetSetting;
	@Value("${updateSettingsQuery}")
	private String qUpdateSettings;
	@Value("${insertSettingsQuery}")
	private String qInsertSettings;
	@Value("${selectDebugQuery}")
	private String qSelectDebug;
	@Value("${getCategoriesQuery}")
	private String qGetCategories;
	
	@Value("${db.driver}")
	private String sDbDriver;
	@Value("${db.url}")
	private String sDbURL;
	@Value("${db.user}")
	private String sDbUser;
	@Value("${db.password}")
	private String sDbPassword;
	
	// Connect to database
	public void connect() {
		try {
			Class.forName(sDbDriver);
			conn = DriverManager.getConnection(sDbURL, sDbUser, sDbPassword);
			stmt = conn.createStatement();
			LOGGER.info("Connected to database...");
		} catch (ClassNotFoundException cnfe) {
			LOGGER.error(cnfe.getMessage());
		} catch (SQLException sqe) {
			LOGGER.error(sqe.getMessage());
		}
	}

	// Insert transaction
	public void insert(double dAmount, String sCategory, String sDescription, String sDate) {
		
		float fAmount = (float) dAmount;
		try {
			cstmt = conn.prepareCall("{CALL FM.F_INSERT_TRANSACTION(?,?,?,?) }");
			cstmt.setFloat(1, fAmount);
			cstmt.setString(2, sCategory);
			cstmt.setString(3, sDescription);
			cstmt.setString(4, sDate);
			cstmt.execute();
		} catch (SQLException sqe) {
			LOGGER.error(sqe.getMessage());
		}
		
	}
	
	// Get expense by categories
	public HashMap<String, HashMap<String, Double>> getPercentages() {
		
		ArrayList<Double> aResultPercentages = new ArrayList<>();
		ArrayList<String> aResultCategories = new ArrayList<>();
		
		HashMap<String, Double> hResult = new HashMap<>();
		
		try {
			cstmt = conn.prepareCall("{CALL FM.F_GET_EXPENSE(?, ?) }");
			cstmt.registerOutParameter(1, Types.ARRAY);
			cstmt.registerOutParameter(2, Types.ARRAY);
			cstmt.executeUpdate();
			Array aPercentages = cstmt.getArray(1);
			Array aCategories = cstmt.getArray(2);
			
			Double[] adPercentages = (Double[])aPercentages.getArray();
			String[] adCategories = (String[])aCategories.getArray();
			
			for (int i = 0; i < adPercentages.length; i++) {
				aResultCategories.add(adCategories[i]);
				aResultPercentages.add(adPercentages[i]);
			}			
			
			for (int i = 0; i < adCategories.length; i++) {
				hResult.put(aResultCategories.get(i), aResultPercentages.get(i));
			}
			
		} catch (SQLException sqe) {
			LOGGER.error(sqe.getMessage());
		}
		
		HashMap<String, HashMap<String, Double>> hmData = new HashMap<>();
		hmData.put("hmPercentages", hResult);
		
		return hmData;
		
	}

	// Calculate total balance from db and return string of rounded result
	public double selectTotalBalance() {	
		
		double dTotalBalance = 0;
		
		try {
			cstmt = conn.prepareCall("{? = CALL FM.F_GET_TOTAL_BALANCE() }");
			cstmt.registerOutParameter(1, Types.DOUBLE);
			cstmt.execute();
			dTotalBalance = cstmt.getDouble(1);
			
			LOGGER.debug(("Total balance: " + dTotalBalance));
		} catch (SQLException sqe) {
			LOGGER.error(sqe.getMessage());
		}

		return dTotalBalance;
	}

	// Calculate monthly balance from db and return string of rounded result
	public double selectMonthlyBalance() {	
		
		double dMonthlyBalance = 0;
		try {
			rs = stmt.executeQuery(qSelectMonthlyBalance);
			while (rs.next()) {
				dMonthlyBalance = rs.getDouble(1);
			}
//			dMonthlyBalance = roundingFormat.format(dMonthlyBalance);
			LOGGER.debug(("Monthly balance: " + dMonthlyBalance));
		} catch (SQLException sqe) {
			LOGGER.error(sqe.getMessage());
		}
		return dMonthlyBalance;
	}
	
	// Calculate monthly budget left from db and return string of rounded result
	public double monthlyBudgetLeft() {	
		
		double dMoneyLeft = 0;
		try {
			rs = stmt.executeQuery(qSelectMonthlyBudget);
			double monthlyExpense = 0;
			while (rs.next()) {
				monthlyExpense = rs.getDouble(1);
			}
			// monthlyExpense je negativen, zato pristejemo
			double dBudget = getMonthlyBudgetSetting();
			dMoneyLeft = dBudget + monthlyExpense;
			
//			dMoneyLeft = roundingFormat.format(dMoneyLeft);
			LOGGER.debug(("Spending money left for this month: " + dMoneyLeft));
		} catch (SQLException sqe) {
			LOGGER.error(sqe.getMessage());
		}
		
		return dMoneyLeft;
	}
	
	// Get monthly budget setting from db and return string of rounded result
	public double getMonthlyBudgetSetting() {
		double dBudgetSetting = 0;
		
		try {
			rs = stmt.executeQuery(qGetBudgetSetting);
			while (rs.next()) {
				dBudgetSetting = rs.getDouble(1);
			}
//			sBudgetSetting = roundingFormat.format(dBudgetSetting);
		} catch (SQLException sqe) {
			LOGGER.error(sqe.getMessage());
		}
		
		return dBudgetSetting;
	}
	
	// Save budget to database
	public void insertSettings(double dBudget) {
		System.out.println(dBudget);
		try {
			pstmt = conn.prepareStatement(qInsertSettings);
			pstmt.setDouble(1, dBudget);
			pstmt.executeUpdate();
		} catch (SQLException sqe) {
			LOGGER.error(sqe.getMessage());
		}
	}
	
	// Get available categories
	public ArrayList<String> getCategories() {
		ArrayList<String> lCategories = new ArrayList<>();
		try {
			rs = stmt.executeQuery(qGetCategories);
			while (rs.next()) {
				lCategories.add(rs.getString(1));
			}
		} catch (SQLException sqe) {
			LOGGER.error("Exception occured while loading categories: " + sqe);
		}
		return lCategories;
	}

	// Select * - for debugging
	public void selectDebug() throws SQLException {
		rs = stmt.executeQuery(qSelectDebug);
		while (rs.next()) {
			LOGGER.debug(rs.getInt(1)
							 + ", " + rs.getString(2)
							 + ", " + rs.getInt(3)
							 + ", " + rs.getString(4));
		}
	}

	// Close database connection
	public void closeDatabaseConnection() {
		try {
			stmt.close();
			if (pstmt != null) {
				pstmt.close();
			}
			conn.close();
			LOGGER.info("Database connection closed.");
		} catch (SQLException sqe) {
			LOGGER.error(sqe.getMessage());
		}
	}
}
