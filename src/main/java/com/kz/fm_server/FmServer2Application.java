package com.kz.fm_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FmServer2Application {

	public static void main(String[] args) {
		SpringApplication.run(FmServer2Application.class, args);
	}
}
