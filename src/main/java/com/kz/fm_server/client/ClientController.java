package com.kz.fm_server.client;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.kz.fm_server.mapper.DataMapper;
import com.kz.fm_server.mapper.UserMapper;

@Controller
public class ClientController {

	private final static Logger LOGGER = Logger.getLogger(ClientController.class.getName());
	
	@Value("${fm.server.url}")
	private String sUrl;
	@Value("${fm.server.url.login}")
	private String sLoginUrl;
	@Value("${fm.server.url.saveTransaction}")
	private String sSaveTransactionUrl;
	@Value("${fm.server.url.getMonthlyBudgetSetting}")
	private String sGetMonthlyBudgetSetting;
	@Value("${fm.server.url.refreshData}")
	private String sRefreshData;
	@Value("${fm.server.url.saveSettings}")
	private String sSaveSettings;
	
	private RestTemplate restTemplate = new RestTemplate();
	
	
	@GetMapping("/cGetMonthlyBudgetSetting")
	@ResponseBody
	public String getMonthlyBudgetSetting() {
		DataMapper dmGetData = restTemplate.getForObject(sUrl + sGetMonthlyBudgetSetting, DataMapper.class);
		
		return String.valueOf(dmGetData.getdMonthlyBudgetSetting());
	}
	
	@PostMapping("/cLogin")
	public ModelAndView login(@RequestParam("username") String sUserName, 
					  		  @RequestParam("password") String sPassword) {
		LOGGER.info("Client login controller ");
		
		UserMapper umSendData = new UserMapper();
		umSendData.setsUserName(sUserName);
		umSendData.setsPassword(sPassword);
		
		UserMapper umGetData = restTemplate.postForObject(sUrl + sLoginUrl, umSendData, UserMapper.class); 
		String sValidate = umGetData.getValidate();
		
		ModelAndView mv = new ModelAndView();
		
		if (sValidate.equals("true")) {
			mv.setViewName("app");
		} else {
			mv.setViewName("index");
			mv.addObject("message", "Wrong credentials!");
		}
		return mv;
	}
	
	@PostMapping("/cSaveTransaction")
	@ResponseBody
	public void cSaveTransaction(@RequestParam("tr_type")     String sTrType,
								 @RequestParam("tr_value")    float  fTrValue,
								 @RequestParam("tr_desc")     String sTrDesc,
								 @RequestParam("tr_category") String sTrCategory,
								 @RequestParam("tr_date")     String sTrDate) {
		LOGGER.info("Client cSaveTransaction controller ");
		
		DateFormat format  = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = format.parse(sTrDate);
		} catch (ParseException pe) {
			LOGGER.warn(pe.getMessage());
		}
		
		String sDate = format1.format(date);
		
		DataMapper dmSendData = new DataMapper();
		dmSendData.setsType(sTrType);
		dmSendData.setfValue(fTrValue);
		dmSendData.setsDescription(sTrDesc);
		dmSendData.setsCategory(sTrCategory);
		dmSendData.setsDate(sDate);
		
		restTemplate.postForObject(sUrl + sSaveTransactionUrl, dmSendData, DataMapper.class);	
	}
	
	@GetMapping("/cRefreshData")
	@ResponseBody
	public HashMap<String, Double> cRefreshData() {
		LOGGER.info("Client cRefreshData controller ");
		DataMapper dmGetData = restTemplate.getForObject(sUrl + sRefreshData, DataMapper.class);
		HashMap<String, Double> hmData = new HashMap<>();
		hmData.put("dTotalBalance",   dmGetData.getdTotalBalance());
		hmData.put("dMonthlyBalance", dmGetData.getdMonthlyBalance());
		hmData.put("dMonthlyBudget",  dmGetData.getdMonthlyBudget());
		return hmData;
	}
	
	@PostMapping("/cSaveSettings")
	@ResponseBody
	public HashMap<String, Double> cSaveSettings(@RequestParam("budget_setting") String sMonthlyBudgetSetting) {
		LOGGER.info("Client cSaveSettings controller");
		HashMap<String, Double> hmData = new HashMap<>();
		DataMapper dmSendData = new DataMapper();
		dmSendData.setdMonthlyBudgetSetting(Double.parseDouble(sMonthlyBudgetSetting));

		DataMapper dmGetData = restTemplate.postForObject(sUrl + sSaveSettings, dmSendData, DataMapper.class);
		hmData.put("dMonthlyBudgetSetting", dmGetData.getdMonthlyBudgetSetting());
		return hmData;
	}
	
	@GetMapping(value = "/")
	public String index() {
		return "index";
	}
	
	@GetMapping(value = "/app")
	public String app() {
		return "app";
	}
	
	@PostMapping("/cLogout")
	public String logout() {
		return "index";
	}
}
