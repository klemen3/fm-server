package com.kz.fm_server.service;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.kz.fm_server.dao.ConnectionPoolDataSource;

@Service
public class LoginService {
	
	Connection conn 		= null;
	Statement stmt 			= null;
	PreparedStatement pstmt = null;

	
	@Value("${queryCheckCredentials}")
	private String qCheckCredentials;
	
	@Autowired
	private ConnectionPoolDataSource dataSource;
	
	/**
	 *  Check for credentials
	 *  
	 *  @param  sUserName
	 *  @param  sPassword
	 *  @return true if username and password matches
	 * 
	 */
	public boolean check(String sUserName, String sPassword) throws SQLException, ClassNotFoundException, PropertyVetoException {		

		conn = dataSource.getConnection();
		
		pstmt = conn.prepareStatement(qCheckCredentials);
		pstmt.setString(1, sUserName);
		pstmt.setString(2, sPassword);
			
		ResultSet rs = pstmt.executeQuery();
		
		String sUser = "";
		String sPass = "";
		
		while (rs.next()) {
			sUser = rs.getString(1);
			sPass = rs.getString(2);
		}
		
		if (sUser.equals(sUserName) && sPass.equals(sPassword)) {
			System.out.println("TRUE");
			return true;
		}
		System.out.println("FALSE");
		return false;
	}
}
