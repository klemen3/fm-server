$( document ).ready(function() {
	openNav();
});

$("#save_button").button();
$("#refresh_button").button();
$("#tr_type").selectmenu();
$("#tr_category").selectmenu();

$(function() {
	$("#tr_date").datepicker();
	$("#tr_date").datepicker("setDate", today);
});


var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1;
var yyyy = today.getFullYear();
if (dd < 10) { dd = '0' + dd }
if (mm < 10) { mm = '0' + mm }
today = mm + '/' + dd + '/' + yyyy;


$(function() {
	$("[name='type_check']").on("change", listenCheck);
});

$(function() {
	$("input[type='radio']").checkboxradio();
});

function listenCheck() {
	if ($('#tr_income').is(":checked")) {
		$("#tr_category").selectmenu({
			disabled : true
		});
	} else {
		console.log('false');
		$("#tr_category").selectmenu({
			disabled : false
		});
	}
}

$(function() {
	$('#refresh_button').click(function() {
		console.log('refresh button clicked');
		$.ajax({
			type : "GET",
			url : "cRefreshData",
			success : function(data) {
				var dTotalBalance   = data["dTotalBalance"];
				var dMonthlyBalance = data["dMonthlyBalance"];
				var dMonthlyBalance = data["dMonthlyBalance"];

				$("#total_balance").text(dTotalBalance);
				$("#monthly_balance").text(dMonthlyBalance);
				$("#money_left").text(dMonthlyBalance);
			}
		});
	});
});

$(function() {
	$('#save_button').click(function() {
		console.log('save button clicked');
		var tr_type;
		if ($('#tr_income').is(":checked")) {
			tr_type = "Income";
		} else {
			tr_type = "Expense";
		}
		
		var tr_value = $("#tr_value").val();
		var tr_desc = $("#tr_description").val();
		var tr_category = $("#tr_category").val();
		var tr_date = $("#tr_date").val();

		$.ajax({
			type : "POST",
			url : "cSaveTransaction",
			data : {
				tr_type : tr_type,
				tr_value : tr_value,
				tr_desc : tr_desc,
				tr_category : tr_category,
				tr_date : tr_date
			},
			success : function() {
				console.log('success.');
			}
		});
	});
});

$("#tr_type").selectmenu({
	change : function() {
		var sValid = $("#tr_type").val();
		if (sValid == "Income") {
			$("#tr_category").selectmenu({
				disabled : true
			});
		} else {
			$("#tr_category").selectmenu({
				disabled : false
			});
		}
	}
});

$(function() {
	var dialog = $("#settings_dialog").dialog({
		autoOpen : false,
		height : 400,
		width : 350,
		modal : true,
		buttons : {
			Save : function() {
				var dBudgetSetting = $("#d_budget_setting").val();
				console.log("Setting: " + dBudgetSetting);
				$.ajax({
					type : "POST",
					url : "cSaveSettings",
					data: {budget_setting: dBudgetSetting},
					success : function() {
						console.log('success.');
					}
				});
				console.log('save');
			},
			Cancel : function() {
				dialog.dialog("close");
			}
		},
		close : function() {
			console.log('close');
		}
	});

	$("#btn_settings").button().on("click", function() {
		dialog.dialog("open");
	});

	$('a[href="#href_settings"]').click(function() {
		dialog.dialog("open");
	});
});




function openNav() {
	document.getElementById("mySidenav").style.width = "170px";
	document.getElementById("main").style.marginLeft = "170px";
}

function closeNav() {
	document.getElementById("mySidenav").style.width = "0";
	document.getElementById("main").style.marginLeft = "0";
}